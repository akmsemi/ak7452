/**
 * This is a device driver for the AK7452 angle sensor with SPI, ABZ, and UVW interfaces.
 *
 * @note AK7452 is a high speed angle sensor IC manufactured by AKM.
 *
 * Example:
 * @code
 * #include "mbed.h"
 * #include "ak7452.h"
 *
 *
 * int main() {
 *     // Example code here
 * }
 * @endcode
 */
#ifndef AK7452_H
#define AK7452_H

#include "mbed.h"

// List of OPCODEs
#define AK7452_OPCODE_WRITE_EEPROM			0b00000001	// Write to EEPROM
#define AK7452_OPCODE_READ_EEPROM			0b00000010	// Read from EEPROM
#define AK7452_OPCODE_WRITE_REGISTER		0b00000011	// Write to register
#define AK7452_OPCODE_READ_REGISTER			0b00000100	// Read from register
#define AK7452_OPCODE_CHANGE_MODE			0b00000101	// Change mode
#define AK7452_OPCODE_ANGLE_DATA_RENEW		0b00001000	// Refresh angle data
#define AK7452_OPCODE_READ_ANGLE			0b00001001	// Read an angle data

// Register Address Map
#define AK7452_REG_ANG_U					0x00		// 14-bit angle data (upper 2 bits)
#define AK7452_REG_ANG_L					0x01		// 14-bit angle data (lower 12 bits)
#define AK7452_REG_MAG						0x02		// Magnetic flux density strength (1LSB/mT)
#define AK7452_REG_CHMD						0x03		// Mode indicator data
#define AK7452_REG_ERRMON					0x04		// Error monitoring register
#define AK7452_REG_ZP_U						0x05		// 14-bit zero degree point setting (upper 2 bits)
#define AK7452_REG_ZP_L						0x06		// 14-bit zero degree point setting (lower 12 bits)
#define AK7452_REG_RDABZ					0x07		// Rotation direction setting
#define AK7452_REG_ABZRES					0x08		// ABZ output resolution setting
#define AK7452_REG_MLK						0x09		// Memory lock indicator
#define AK7452_REG_SDDIS					0x0A		// For set up abnormal detection disable
#define AK7452_REG_UVW						0x0B		// For set up "UVW output enable disable"

// EEPROM address map
#define AK7452_EEPROM_ID1					0x04		// User memory
#define AK7452_EEPROM_ID2					0x08		// User memory
#define AK7452_EEPROM_ZP_U					0x14		// 14-bit zero degree point (upper 2 bits)
#define AK7452_EEPROM_ZP_L					0x18		// 14-bit zero degree point (lower 12 bits)
#define AK7452_EEPROM_RDABZ					0x1C		// 14-bit zero degree point (lower 12 bits)
#define AK7452_EEPROM_ABZRES				0x20		// ABZ output resolution setting
#define AK7452_EEPROM_MLK					0x24		// Memory lock key
#define AK7452_EEPROM_SDDIS					0x28		// Self-diagnostic on/off setting
#define AK7452_EEPROM_UVW					0x2C		// UVW output on/off setting, output hysteresis setting, and output resolution setting
#define AK7452_EEPROM_OFSTX					0x40		// X-axis offset correction setting
#define AK7452_EEPROM_OFSTY					0x44		// Y-axis offset correction setting
#define AK7452_EEPROM_ORTH					0x48		// Orthogonality error correction setting
#define AK7452_EEPROM_GM					0x4C		// Gain mismatch correction setting

#define AK7452_BIT_MASK_ABZ_E               0x80		// ABZ output enable
#define AK7452_BIT_MASK_UVW_E               0x40		// UVW output enable

#define AK7452_ABNORMAL_STATE_NORMAL        0x03		// Abnormal state error code

#define AK7452_MODE_NORMAL					0x000
#define AK7452_MODE_USER					0x50F

// Masks
#define AK7452_BIT_MASK_MODE				0b00000010
#define AK7452_BIT_MASK_PARITY1				0b00000001
#define AK7452_BIT_MASK_PARITY2				0b10000000
#define AK7452_BIT_MASK_ERROR				0b01000000
#define AK7452_BIT_MASK_ANGLE_H				0b00111111
#define AK7452_BIT_MASK_MAG_H				0b00000011
#define AK7452_BIT_MASK_ERRMON				0b00000011
#define AK7452_BIT_MASK_ABZ_E               0x80		// ABZ output enable
#define AK7452_BIT_MASK_UVW_E               0x40		// UVW output enable

// Offsets (8-bit)
#define AK7452_BIT_OFFSET_ERROR				6
#define AK7452_BIT_OFFSET_PARITY1			0
#define AK7452_BIT_OFFSET_PARITY2			7
#define AK7452_BIT_OFFSET_MODE				1

#define AK7452_LEN_BUF_MAX					0x03		// 2 byte maximum, plus address byte


class AK7452
{
public:

    /**
     * Available opration modes in AK7452.
     */
    typedef enum {
        AK7452_NORMAL_MODE  = 0x0000,   /**< Normal mode operation. */
        AK7452_USER_MODE    = 0x050F,   /**< User mode operation. */
    } OperationMode;

    /**
     * Status of function.
     */
    typedef enum {
        SUCCESS,                    /**< The function processed successfully. */
        ERROR,                      /**< General Error */
        ERROR_IN_USER_MODE,         /**< Error in user mode. */
        ERROR_IN_NORMAL_MODE,       /**< Error in normal mode. */
        ERROR_PARITY,               /**< Parity bit error. */
        ERROR_ABNORMAL_STRENGTH,    /**< Abnormal strength error. */
    } Status;

    /**
     * Constructor.
     *
     */
    AK7452();

    /**
     * Destructor.
     *
     */
     ~AK7452();

    /**
     * begin
     *
     * @param *spi pointer to SPI instance
     * @param *cs pointer to DigitalOut instance for CS
     */
    void begin(SPI *spi, DigitalOut *cs);

    /**
     * Writes data to EEPROM on the device.
     * @param address EEPROM address
     * @param data data to be written
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status writeEEPROM(char address, const char *data);

    /**
     *  Reads data from EEPROM on the device.
     * @param address EEPROM address
     * @param data data to read
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status readEEPROM(char address, char *data);

    /**
     * Writes data to register on the device.
     * @param address register address
     * @param data data to be written
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status writeRegister(char address, const char *data);

    /**
     *  Reads data from register on the device.
     * @param address register address
     * @param data data to read
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status readRegister(char address, char *data);

    /**
     * Sets device operation mode.
     *
     * @param mode device opration mode
     *
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status setOperationMode(OperationMode mode);

    /**
     * Gets device operation mode.
     *
     * @return Returns OperationMode.
     */
    OperationMode getOperationMode();

    /**
     * Reads angle data from the device.
     *
     * @param angle pointer to read angle data buffer
     *
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status readAngle(char *angle);

    /**
     * Measures and reads angle, magnetic flux density and abnormal state code while in the user mode.
     *
     * @param angle pointer to angle data buffer
     * @param density magnetic flux density
     * @param abnormal_state abnormal state
     *
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status readAngleFluxState(char *angle, char *density, char *abnormal_state);

    /**
     * Measures current angle and sets the value to EEPROM as zero angle position.
     *
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status setAngleZero();

    /**
     * Sets the value to EEPROM as zero angle position.
     *
     * @param angle zero angle position
     *
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status setAngleZero(const char *angle);

private:
    /**
     * Holds a pointer to an SPI object.
     */
    SPI *_spi;

    /**
     * Holds a DigitalOut oblject for CS;
     */
    DigitalOut *_cs;

    /**
     * Holds current mode
     */
    OperationMode operationMode;

    /**
     *  Reads data from device.
     * @param operation_code OPCODE
     * @param address memory/register address
     * @param *data pointer to the read buffer, fixed length of 2.
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status read(char operation_code, char address, char *data);

    /**
     * Writes data to the device.
     * @param operation_code OPCODE
     * @param address memory/register addredd
     * @param *data pointer to the read buffer. length=2 fixed.
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status write(char operation_code, char address, const char *data);

    /**
     * Writes data to the device.
     * @param operation_code OPCODE
     * @param address memory/register addredd
     * @param *data pointer to the read buffer. length=2 fixed.
     * @return Returns SUCCESS when succeeded, otherwise returns a failure code.
     */
    Status renew();

    Status parityCheck(const char* data);

};

#endif
