#include "ak7452.h"
#include "Debug.h"


/**
 * Constructor.
 *
 */
AK7452::AK7452(){
    _spi = NULL;
    _cs = NULL;
    operationMode = AK7452_NORMAL_MODE;
}

/**
 * Destructor.
 *
 */
AK7452::~AK7452(){
    if (_spi) delete _spi;
    if(_cs) delete _cs;
}

/**
 * begin
 *
 * @param *spi pointer to SPI instance
 * @param *cs pointer to DigitalOut instance for CS
 */
void AK7452::begin(SPI *spi, DigitalOut *cs){
    if (_spi) delete _spi;
    if (_cs) delete _cs;

    _spi=spi;
    _cs=cs;

    _cs->write(1);
}

/**
 * Writes data to EEPROM on the device.
 * @param address EEPROM address
 * @param data data to be written
 * @return Returns SUCCESS when succeeded, otherwise returns another.
 */
AK7452::Status AK7452::writeEEPROM(char address, const char *data){

	Status status;

	if(operationMode != AK7452_USER_MODE)
		return ERROR_IN_NORMAL_MODE;

	status = write(AK7452_OPCODE_WRITE_EEPROM, address, data);
    wait(0.01);    // 10ms delay for safe write

    return status;
}

/**
 *  Reads data from EEPROM on the device.
 * @param address EEPROM address
 * @param data data to read
 * @return Returns SUCCESS when succeeded, otherwise returns another.
 */
AK7452::Status AK7452::readEEPROM(char address, char *data){

	Status status;

	if(operationMode != AK7452_USER_MODE)
		return ERROR_IN_NORMAL_MODE;

	status = read(AK7452_OPCODE_READ_EEPROM, address, data);

	return status;
}

/**
 * Writes data to register on the device.
 * @param address register address
 * @param data data to be written
 * @return Returns SUCCESS when succeeded, otherwise returns another.
 */
AK7452::Status AK7452::writeRegister(char address, const char *data){

	Status status;

	if(operationMode != AK7452_USER_MODE)
		return ERROR_IN_NORMAL_MODE;

    status = write(AK7452_OPCODE_WRITE_REGISTER, address, data);

    return status;
}

/**
 *  Reads data from register on the device.
 * @param address register address
 * @param data data to read
 * @return Returns SUCCESS when succeeded, otherwise returns another.
 */
AK7452::Status AK7452::readRegister(char address, char *data){

	Status status;

	if(operationMode != AK7452_USER_MODE)
    	return ERROR_IN_NORMAL_MODE;

    status = read(AK7452_OPCODE_READ_REGISTER, address, data);

    return status;
}

/**
 * Sets device operation mode.
 *
 * @param mode device opration mode
 *
 * @return Returns SUCCESS when succeeded, otherwise returns another code.
 */
AK7452::Status AK7452::setOperationMode(AK7452::OperationMode mode){

	Status status;
	char command[2];

    command[0] = (char)(0x0F & (mode>>8));	// R_CHMD[11:8]
    command[1] = (char)(0x00FF & mode);		// R_CHMD[7:0]

    operationMode = mode;
    status = write(AK7452_OPCODE_CHANGE_MODE, AK7452_REG_CHMD, command);

    wait(0.01);		// 10ms

    return status;
}

/**
 * Gets device operation mode.
 *
 * @return Returns OperationMode.
 */
AK7452::OperationMode AK7452::getOperationMode(){
    return AK7452::operationMode;
}

/**
 * Reads angle data from the device.
 *
 * @param ang_data Reference to read angle data buffer array
 *
 * @return Returns SUCCESS when succeeded, otherwise returns another code.
 */
AK7452::Status AK7452::readAngle(char* angle) {

    char data[3] = {0x00, 0x00, 0x00};

    AK7452::read(AK7452_OPCODE_READ_ANGLE, 0x00, data);

    // Check that AK7452 is in Normal Mode
    if( (data[0] & AK7452_BIT_MASK_MODE) != 0 ){
//        AK7452::setOperationMode(AK7452::AK7452_NORMAL_MODE);
    	DBG_L1("#Error: Cannot read angle in User Mode.\r\n");
        return AK7452::ERROR_IN_USER_MODE;
    }else if( (data[1] & AK7452_BIT_MASK_ERROR) == 0 ){
    	DBG_L1("#Error: Abnormal strength.\n");
        return AK7452::ERROR_ABNORMAL_STRENGTH;
    }

    // parity check
    if( AK7452::parityCheck(data) != AK7452::SUCCESS ){
    	DBG_L1("#Error: Failed parity check\r\n");
    	return AK7452::ERROR_PARITY;
    }

    // Status check passed, arrange 14-bit angle data
    angle[0] = (data[1] & AK7452_BIT_MASK_ANGLE_H);
    angle[1] = data[2];

    return AK7452::SUCCESS;
}

/**
 * Measures and reads angle, magnetic flux density and abnormal state code while in the user mode.
 *
 * @param angle pointer to angle data
 * @param density magnetic flux density
 * @param abnormal_state abnormal state
 *
 * @return Returns SUCCESS when succeeded, otherwise returns another code.
 */
AK7452::Status AK7452::readAngleFluxState(char* angle, char* density, char* abnormal_state) {

	Status status;
	char data[3] = {0x00, 0x00, 0x00};

	if(operationMode != AK7452_USER_MODE)
		return ERROR_IN_NORMAL_MODE;

    // Refresh the angle data
    status = renew();		// Refresh the angle data
    if(status != SUCCESS) return status;

    wait(0.01);

    // Read the angle
    status = readRegister(AK7452_REG_MAG, data);
    if(status != SUCCESS) return status;

    angle[0] = (data[1] & AK7452_BIT_MASK_ANGLE_H);
    angle[1] = data[2];

    // Read the magnetic flux density
    status = readRegister(AK7452_REG_MAG, data);
    if(status != SUCCESS) return status;

    density[0] = (data[1] & AK7452_BIT_MASK_MAG_H);
    density[1] = data[2];

    // Read the abnormal state code
    status = readRegister(AK7452_REG_ERRMON, data);
    if(status != SUCCESS) return status;

    *abnormal_state = (data[2] & AK7452_BIT_MASK_ERRMON);

    return AK7452::SUCCESS;
}

/**
 * Measures current angle and sets the value to EEPROM as zero angle position.
 *
 * @return Returns SUCCESS when succeeded, otherwise returns another code.
 */
 AK7452::Status AK7452::setAngleZero(){

    Status status = SUCCESS;

    char angle[2] = {0x00, 0x00};
    char density[2] = {0x00, 0x00};
    char abnormal_state;

    // Store current mode for later
    OperationMode original_mode = operationMode;

    // set to user mode
    status = setOperationMode(AK7452_USER_MODE);
    if(status != SUCCESS) return status;

    // Initialize zero point to 0
    status = writeRegister(AK7452_EEPROM_ZP_U, 0x00);
    if(status != SUCCESS) return status;

    status = writeRegister(AK7452_EEPROM_ZP_L, 0x00);
    if(status != SUCCESS) return status;

    // Read angle data with zero ZP offset
    status = readAngleFluxState(angle, density, &abnormal_state);
    if(status != SUCCESS) return status;

    if(abnormal_state != AK7452_ABNORMAL_STATE_NORMAL){
    	return ERROR_ABNORMAL_STRENGTH;
    }

    // Set current angle to zero point
    status = setAngleZero(angle);
    if(status != SUCCESS) return status;

    // Return to previous mode
    status = setOperationMode(original_mode);
    if(status != SUCCESS) return status;

    return status;
}

/**
 * Sets the value to EEPROM as zero angle position.
 *
 * @param angle zero angle position
 *
 * @return Returns SUCCESS when succeeded, otherwise returns another code.
 */
AK7452::Status AK7452::setAngleZero(const char *angle){
//    if(AK7452::operationMode != AK7452::AK7452_USER_MODE) return AK7452::ERROR_IN_NORMAL_MODE;

	char zp_u[2] = {0x00, 0x00};
	char zp_l[2] = {0x00, 0x00};

	zp_u[1] = (angle[0] >> 4);
	zp_l[0] = angle[1] & AK7452_BIT_MASK_ANGLE_H;
	zp_l[1] = angle[2];

	writeRegister(AK7452_REG_ZP_U, zp_u);
    writeRegister(AK7452_REG_ZP_L, zp_l);

    // Write the upper zero point
    if(writeEEPROM(AK7452_REG_ZP_U, zp_u) != SUCCESS)
    	return ERROR;

    // Write the lower zero point
    if(writeEEPROM(AK7452_REG_ZP_L, zp_l) != SUCCESS)
    	return ERROR;

    return SUCCESS;
}

////////////////////////////////////////////////////////
// private methods
////////////////////////////////////////////////////////

/**
 * Reads data from device.
 * @param operation_code OPCODE
 * @param address memory/register addredd
 * @param *data pointer to the read buffer. length=2 fixed.
 * @return Returns SUCCESS when succeeded, otherwise returns another.
 */
AK7452::Status AK7452::read(char operation_code, char address, char *data)
{
	char command[3];

	command[0] = (operation_code << 4) | (address >> 3);
    command[1] = 0xF0 & (address << 5);
    command[2] = 0x00;

	_cs->write(0);

	for(int i = 0; i < 3; i++){
		data[i] = _spi->write(command[i]);
	}

	_cs->write(1);

    return SUCCESS;
}

/**
 * Writes data to the device.
 * @param operation_code OPCODE
 * @param address memory/register addredd
 * @param *data pointer to the read buffer. length=2 fixed.
 * @return Returns SUCCESS when succeeded, otherwise returns another.
 */
AK7452::Status AK7452::write(char operation_code, char address, const char *data)
{
	char command[3];

	command[0] = (operation_code<<4) | (address>>3);
    command[1] = (0xF0 & (address<<5)) | (0x0F & data[0]);
    command[2] = data[1];

    _cs->write(0);

    for(int i=0; i<3; i++){
        _spi->write(command[i]);
    }

    _cs->write(1);

    return SUCCESS;
}

AK7452::Status AK7452::renew()
{
	char command = (AK7452_OPCODE_ANGLE_DATA_RENEW << 4);

    _cs->write(0);
    _spi->write(command);
    _cs->write(1);

    return SUCCESS;
}

AK7452::Status AK7452::parityCheck(const char* data)
{
	char p1, p2;
	char error, sum;
	char hi_data, lo_data;

	p1 = (data[0] & AK7452_BIT_MASK_PARITY1) >> AK7452_BIT_OFFSET_PARITY1;
	p2 = (data[1] & AK7452_BIT_MASK_PARITY2) >> AK7452_BIT_OFFSET_PARITY2;

	error = (data[1] & AK7452_BIT_MASK_ERROR) >> AK7452_BIT_OFFSET_ERROR;

	// hi_data = R_ANG[13:7]
	hi_data = (data[1] & AK7452_BIT_MASK_ANGLE_H) << 1;
	hi_data |= (data[2] & 0b10000000) >> 7;

	// lo_data = R_ANG[6:0]
	lo_data = (data[2] & 0b01111111);

	// Calculate parity 1
	sum = p1 + error;

    for(int i = 0; i < 7; i++){
        sum += ((hi_data >> i) & 0x01);
    }

    // Check parity of P1
    if((sum % 2) != 0){
    	DBG_L1("#Error: Parity check of P1 failed.\r\n");
    	return ERROR_PARITY;
    }

    // Calculate parity 2
    sum = p2 + error;

    for(int i = 0; i < 7; i++){
		sum += ((lo_data >> i) & 0x01);
	}

    // Check parity of P2
    if((sum % 2) != 0){
    	DBG_L1("Error: Parity check of P2 failed.\r\n");
    	return ERROR_PARITY;
    }

	return SUCCESS;;
}
